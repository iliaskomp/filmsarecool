package com.iliaskomp.filmsarecool.data.repository;

import com.iliaskomp.filmsarecool.data.network.response.PopularFilmResponse;
import com.iliaskomp.filmsarecool.data.network.response.PopularFilmsResponse;
import com.iliaskomp.filmsarecool.data.network.retrofit.TmdbService;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import io.reactivex.Flowable;
import io.reactivex.observers.TestObserver;

public class FilmsRepositoryImplTest {

    private TmdbService mockService = Mockito.mock(TmdbService.class);
    private FilmsRepository repo = new FilmsRepositoryImpl(mockService);


    @Test
    public void test() {
        PopularFilmsResponse response = new PopularFilmsResponse();
        response.setTotalPages(10);

        Mockito.when(mockService.popularFilms(1)).thenReturn(Flowable.just(response));

        TestObserver observer = TestObserver.create();

        repo.getPopularFilms(1).subscribe(observer);

        observer.assertNoErrors();
//        observer.assertComplete();
        observer.assertValueCount(1);
        //        observer.assertNotComplete()
        //        observer.assertNoErrors()
        //        observer.assertValueCount(4)
    }
}
