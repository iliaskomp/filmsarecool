package com.iliaskomp.filmsarecool

import android.app.Application
import com.iliaskomp.filmsarecool.config.BASE_URL

import com.iliaskomp.filmsarecool.di.component.DaggerNetComponent
import com.iliaskomp.filmsarecool.di.component.DaggerRepositoryComponent
import com.iliaskomp.filmsarecool.di.component.NetComponent
import com.iliaskomp.filmsarecool.di.component.RepositoryComponent
import com.iliaskomp.filmsarecool.di.module.AppModule
import com.iliaskomp.filmsarecool.di.module.NetModule

import timber.log.Timber

class App : Application() {

    lateinit var netComponent: NetComponent
        private set
    lateinit var repositoryComponent: RepositoryComponent
        private set

    override fun onCreate() {
        super.onCreate()

        netComponent = DaggerNetComponent.builder()
                .appModule(AppModule(this))
                .netModule(NetModule(BASE_URL))
                .build()

        repositoryComponent = DaggerRepositoryComponent.builder()
                .build()

        initTimber()
    }

    private fun initTimber() {
        Timber.plant(Timber.DebugTree())
    }
}
