package com.iliaskomp.filmsarecool.di.module

import android.app.Application

import com.iliaskomp.filmsarecool.di.scopes.ApplicationScope


import dagger.Module
import dagger.Provides

@Module
class AppModule(private val mApplication: Application) {

    @Provides
    @ApplicationScope
    internal fun application() = mApplication

}
