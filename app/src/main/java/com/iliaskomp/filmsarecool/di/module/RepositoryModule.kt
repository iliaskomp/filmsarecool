package com.iliaskomp.filmsarecool.di.module

import com.iliaskomp.filmsarecool.di.scopes.ApplicationScope
import com.iliaskomp.filmsarecool.data.network.retrofit.TmdbService
import com.iliaskomp.filmsarecool.data.repository.FilmsRepository
import com.iliaskomp.filmsarecool.data.repository.FilmsRepositoryImpl

import dagger.Module
import dagger.Provides

@Module(includes = [NetModule::class])
class RepositoryModule {

    @Provides
    @ApplicationScope
    internal fun filmsRepository(tmdbService: TmdbService) = FilmsRepositoryImpl(tmdbService)
}
