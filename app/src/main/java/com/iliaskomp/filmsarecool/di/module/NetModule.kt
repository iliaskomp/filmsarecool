package com.iliaskomp.filmsarecool.di.module


import android.app.Application

import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.iliaskomp.filmsarecool.config.*
import com.iliaskomp.filmsarecool.di.scopes.ApplicationScope
import com.iliaskomp.filmsarecool.data.network.retrofit.TmdbService
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory

import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
class NetModule(private val mBaseUrl: String) {

    @Provides
    @ApplicationScope
    internal fun httpCache(application: Application): Cache {
        val cacheSize = 10 * 1024 * 1024
        return Cache(application.cacheDir, cacheSize.toLong())
    }

    @Provides
    @ApplicationScope
    internal fun gson() = GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .serializeNulls()
            .create()


    @Provides
    @ApplicationScope
    internal fun okhttpClient(cache: Cache, apiKeyInterceptor: Interceptor): OkHttpClient {
        val client = OkHttpClient.Builder()
        client.cache(cache)
        client.addInterceptor(apiKeyInterceptor)
        return client.build()
    }

    @Provides
    @ApplicationScope
    internal fun apiKeyInterceptor() = Interceptor { chain ->
        val originalRequest = chain.request()
        val originalHttpUrl = originalRequest.url()
        val url = originalHttpUrl.newBuilder()
                .addQueryParameter("api_key", API_KEY)
                .build()

        val newRequest = originalRequest
                .newBuilder()
                .url(url)
                .build()

        chain.proceed(newRequest)
    }

    @Provides
    @ApplicationScope
    internal fun retrofit(gson: Gson, okHttpClient: OkHttpClient) = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(mBaseUrl)
            .client(okHttpClient)
            .build()

    @Provides
    @ApplicationScope
    internal fun tmdbService(retrofit: Retrofit) = retrofit.create(TmdbService::class.java)

}
