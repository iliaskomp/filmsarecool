package com.iliaskomp.filmsarecool.di.scopes

import java.lang.annotation.RetentionPolicy

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ApplicationScope
