package com.iliaskomp.filmsarecool.di.component


import com.iliaskomp.filmsarecool.di.module.RepositoryModule
import com.iliaskomp.filmsarecool.di.scopes.ApplicationScope
import com.iliaskomp.filmsarecool.data.repository.FilmsRepository

import dagger.Component

@ApplicationScope
@Component(modules = [RepositoryModule::class])
interface RepositoryComponent {

    fun inject(repo: FilmsRepository)

}
