package com.iliaskomp.filmsarecool.di.component


import com.iliaskomp.filmsarecool.di.module.AppModule
import com.iliaskomp.filmsarecool.di.module.NetModule
import com.iliaskomp.filmsarecool.di.scopes.ApplicationScope
import com.iliaskomp.filmsarecool.ui.MainActivity


import dagger.Component

@ApplicationScope
@Component(modules = [AppModule::class, NetModule::class])
interface NetComponent {

    fun inject(activity: MainActivity)

}
