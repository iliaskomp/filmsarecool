package com.iliaskomp.filmsarecool.utils

import android.annotation.SuppressLint

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Date

import timber.log.Timber

object DateUtils {

    @SuppressLint("SimpleDateFormat")
    fun convertTmdbDateToYear(fullDate: String): String {
        val oldDateFormat = SimpleDateFormat("yyyy-mm-dd")
        val oldDate: Date
        try {
            oldDate = oldDateFormat.parse(fullDate)
        } catch (e: ParseException) {
            Timber.w(e, "Could not correctly parse tmdb date: %s", fullDate)
            return ""
        }

        val newDateFormat = SimpleDateFormat("yyyy")
        return newDateFormat.format(oldDate)
    }
}
