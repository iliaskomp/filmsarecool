package com.iliaskomp.filmsarecool.data.network.retrofit

import com.iliaskomp.filmsarecool.data.network.response.PopularFilmsResponse

import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Query

interface TmdbService {

    @GET("3/movie/popular?page=2")
    fun popularFilms(@Query("page") page: Int): Flowable<PopularFilmsResponse>
}
