package com.iliaskomp.filmsarecool.data.network.response

import com.google.gson.annotations.SerializedName

class PopularFilmsResponse {

    @SerializedName("page")
    var currentPage: Int = 0

    @SerializedName("total_results")
    var totalResults: Int = 0

    @SerializedName("total_pages")
    var totalPages: Int = 0

    @SerializedName("results")
    var popularFilmResponses: List<PopularFilmResponse>? = null
        internal set
}
