package com.iliaskomp.filmsarecool.data.repository

import com.iliaskomp.filmsarecool.data.model.PopularFilm
import com.iliaskomp.filmsarecool.data.network.response.ConvertUtils
import com.iliaskomp.filmsarecool.data.network.response.PopularFilmsResponse
import com.iliaskomp.filmsarecool.data.network.retrofit.TmdbService


import javax.inject.Inject

import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class FilmsRepositoryImpl (private val tmdbService: TmdbService) : FilmsRepository {

    override fun getPopularFilms(page: Int): Single<List<PopularFilm>> {
        return tmdbService.popularFilms(page)
                .subscribeOn(Schedulers.io())
                .map(PopularFilmsResponse::popularFilmResponses)
                .flatMapIterable { responses -> responses }
                .map(ConvertUtils.Companion::convertToPopularFilm)
                .toList()
    }
}