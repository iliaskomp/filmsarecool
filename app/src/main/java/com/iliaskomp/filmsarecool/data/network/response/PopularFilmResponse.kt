package com.iliaskomp.filmsarecool.data.network.response

import com.google.gson.annotations.SerializedName

data class PopularFilmResponse(
        @SerializedName("vote_count") val voteCount: Int,
        @SerializedName("id") val id: Int,
        @SerializedName("video") val videoAvailable: Boolean,
        @SerializedName("vote_average") val voteAvg: Float,
        @SerializedName("title") val title: String,
        @SerializedName("popularity") val popularity: Float,
        @SerializedName("poster_path") val posterPath: String? = "",
        @SerializedName("original_language") val originalLanguage: String,
        @SerializedName("original_title") val originalTitle: String,
        @SerializedName("backdrop_path") val backdropPath: String? = "",
        @SerializedName("adult") val adult: Boolean,
        @SerializedName("overview") val overview: String,
        @SerializedName("release_date") val releaseDate: String
)
