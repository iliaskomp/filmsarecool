package com.iliaskomp.filmsarecool.data.repository

import com.iliaskomp.filmsarecool.data.model.PopularFilm

import io.reactivex.Single

interface FilmsRepository {

    fun getPopularFilms(page: Int): Single<List<PopularFilm>>
}
