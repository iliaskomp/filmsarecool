package com.iliaskomp.filmsarecool.data.network.response

import com.iliaskomp.filmsarecool.data.model.PopularFilm

class ConvertUtils {

    companion object {
        fun convertToPopularFilm(response: PopularFilmResponse) =
                PopularFilm(response.voteCount,
                        response.id,
                        response.videoAvailable,
                        response.voteAvg,
                        response.title,
                        response.popularity,
                        response.posterPath,
                        response.originalLanguage,
                        response.originalTitle,
                        response.backdropPath,
                        response.adult,
                        response.overview,
                        response.releaseDate)
    }
}