package com.iliaskomp.filmsarecool.data.model


data class PopularFilm(
        val voteCount: Int,
        val id: Int,
        val videoAvailable: Boolean,
        val voteAvg: Float,
        val title: String,
        val popularity: Float,
        val posterPath: String?,
        val originalLanguage: String,
        val originalTitle: String,
        val backdropPath: String?,
        val adult: Boolean,
        val overview: String,
        val releaseDate: String
)