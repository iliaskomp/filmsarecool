package com.iliaskomp.filmsarecool.ui.popularfilms

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.iliaskomp.filmsarecool.BR
import com.iliaskomp.filmsarecool.R
import com.iliaskomp.filmsarecool.databinding.FragmentPopularFilmsBinding
import com.iliaskomp.filmsarecool.ui.base.BaseFragment
import com.iliaskomp.filmsarecool.ui.base.EndlessRecyclerViewScrollListener
import com.iliaskomp.filmsarecool.ui.base.ViewModelProviderFactory

import java.util.ArrayList

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.processors.PublishProcessor

class PopularFilmsFragment : BaseFragment<PopularFilmsViewModel, FragmentPopularFilmsBinding>() {

    private lateinit var mAdapter: RecyclerView.Adapter<*>
    private lateinit var scrollListener: EndlessRecyclerViewScrollListener
    private val popularFilms = ArrayList<PopularFilmItemViewModel>()

    override val layoutId = R.layout.fragment_popular_films
    override val bindingVariable = BR.viewModel
    override val toolbarTitleId: Int = R.string.popular_films_title


    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        initRecyclerView()
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observePopularFilms()
    }

    override fun createViewModel(): PopularFilmsViewModel {
        val factory = ViewModelProviderFactory(PopularFilmsViewModel(this))
        viewModel = ViewModelProviders.of(this, factory).get(PopularFilmsViewModel::class.java)
        return viewModel
    }

    /* Layout =================================================================================== */
    private fun initRecyclerView() {
        val layoutManager = GridLayoutManager(mainActivity, 2)
        scrollListener = initScrollListener(layoutManager)

        val recyclerView = binding.rvPopularFilms
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = layoutManager

        mAdapter = PopularFilmsAdapter(popularFilms)
        recyclerView.adapter = mAdapter
        recyclerView.addOnScrollListener(scrollListener)
    }


    private fun initScrollListener(layoutManager: LinearLayoutManager): EndlessRecyclerViewScrollListener {
        return object : EndlessRecyclerViewScrollListener(layoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                viewModel.setPage(page + 1) // page starts at zero at the listener but at one for api
            }
        }
    }

    private fun observePopularFilms() {
        viewModel.setPage(1)
        viewModel.films
                .doOnNext { films -> popularFilms.addAll(films) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { _ -> mAdapter!!.notifyDataSetChanged() },
                        { it.printStackTrace() }
                )
    }
}
