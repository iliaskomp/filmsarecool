package com.iliaskomp.filmsarecool.ui.base

import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.iliaskomp.filmsarecool.data.network.retrofit.TmdbService
import com.iliaskomp.filmsarecool.data.repository.FilmsRepository
import com.iliaskomp.filmsarecool.ui.MainActivity
import com.iliaskomp.filmsarecool.ui.Navigator

import timber.log.Timber


abstract class BaseFragment<V : BaseViewModel<*>, B : ViewDataBinding> :
        Fragment(),
        BaseViewModelListener {

    protected lateinit var viewModel: V
    protected lateinit var binding: B

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.d("onCreate() called for: %s", this.javaClass.simpleName)
        viewModel = createViewModel()
        mainActivity.setToolbarTitle(getString(toolbarTitleId))
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {


        binding = DataBindingUtil.inflate(inflater, layoutId, container, false)
        binding.setVariable(bindingVariable, viewModel)
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        viewModel.onStart()
    }

    override fun onStop() {
        super.onStop()
        viewModel.onStop()
    }

    /* Abstract methods */
    abstract fun createViewModel(): V

    @get:LayoutRes
    abstract val layoutId: Int

    abstract val bindingVariable: Int

    abstract val toolbarTitleId: Int

    /* Getters =================================================================================*/
    protected val mainActivity: MainActivity
        get() {
            if (activity is MainActivity) {
                return activity as MainActivity
            }
            throw IllegalStateException("Activity should be an instance of: " + MainActivity.TAG)
        }

    override val navigator: Navigator
        get() = mainActivity.navigator

    override val tmdbService: TmdbService
        get() = mainActivity.tmdbService

    override val filmsRepository: FilmsRepository
        get() = mainActivity.filmsRepository
}
