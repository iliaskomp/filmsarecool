package com.iliaskomp.filmsarecool.ui

import com.iliaskomp.filmsarecool.ui.base.BaseFragment

interface Navigator {

    fun navigateTo(fragment: BaseFragment<*, *>)

    fun navigateToPopularFilms()

}
