package com.iliaskomp.filmsarecool.ui

import androidx.databinding.BindingAdapter
import android.os.Build
import android.view.View
import android.view.View.*
import androidx.annotation.RequiresApi
import android.widget.ImageView

import com.iliaskomp.filmsarecool.R
import com.squareup.picasso.Picasso

@BindingAdapter("imageUrl")
fun ImageView.loadImage(url: String?) {
    if (url == null) {
        Picasso.with(context)
                .load(R.drawable.no_poster_placeholder)
                .into(this)
    } else {
        Picasso.with(context)
                .load(url)
                .placeholder(R.drawable.no_poster_placeholder)
                .into(this)
    }
}

@BindingAdapter("visibleOrGone")
fun View.setVisibleOrGone(show: Boolean) {
    visibility = if (show) VISIBLE else GONE
}

@BindingAdapter("visible")
fun View.setVisible(show: Boolean) {
    visibility = if (show) VISIBLE else INVISIBLE
}
