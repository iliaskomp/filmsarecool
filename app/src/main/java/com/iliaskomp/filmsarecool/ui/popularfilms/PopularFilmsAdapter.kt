package com.iliaskomp.filmsarecool.ui.popularfilms

import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

import com.iliaskomp.filmsarecool.R
import com.iliaskomp.filmsarecool.databinding.ItemPopularFilmBinding

class PopularFilmsAdapter(private val films: List<PopularFilmItemViewModel>) :
        RecyclerView.Adapter<PopularFilmsAdapter.PopularFilmItemViewHolder>() {

    @NonNull
    override fun onCreateViewHolder(@NonNull parent: ViewGroup, viewType: Int): PopularFilmItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil
                .inflate<ItemPopularFilmBinding>(inflater, R.layout.item_popular_film, parent, false)
        return PopularFilmItemViewHolder(binding)
    }

    override fun onBindViewHolder(@NonNull holder: PopularFilmItemViewHolder, position: Int) {
        val film = films[position]
        holder.bind(film)
    }

    override fun getItemCount() = films.size


    /* ========================================================================================== */
    /* View holder ============================================================================== */
    /* ========================================================================================== */

    inner class PopularFilmItemViewHolder(val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(film: PopularFilmItemViewModel) {
            val itemBinding = binding as ItemPopularFilmBinding
            itemBinding.film = film
            itemBinding.executePendingBindings()
        }
    }

    companion object {
        private val TAG = PopularFilmsAdapter::class.java.name
    }

}
