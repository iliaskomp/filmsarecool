package com.iliaskomp.filmsarecool.ui.base

import com.iliaskomp.filmsarecool.data.network.retrofit.TmdbService
import com.iliaskomp.filmsarecool.data.repository.FilmsRepository
import com.iliaskomp.filmsarecool.ui.Navigator

interface BaseViewModelListener {

    val navigator: Navigator

    val tmdbService: TmdbService

    val filmsRepository: FilmsRepository
}
