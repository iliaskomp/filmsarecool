package com.iliaskomp.filmsarecool.ui.popularfilms

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable

import com.iliaskomp.filmsarecool.data.model.PopularFilm
import com.iliaskomp.filmsarecool.utils.DateUtils

class PopularFilmItemViewModel(private val film: PopularFilm) : BaseObservable() {

    val title: String
        @Bindable
        get() = film.title

    val releaseYear: String
        @Bindable
        get() = DateUtils.convertTmdbDateToYear(film.releaseDate)

    //todo move it to a constant
    val posterUrl: String
        @Bindable
        get() = "https://image.tmdb.org/t/p/w500" + film.posterPath!!
}
