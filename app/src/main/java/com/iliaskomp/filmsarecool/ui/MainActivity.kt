package com.iliaskomp.filmsarecool.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar

import com.iliaskomp.filmsarecool.App
import com.iliaskomp.filmsarecool.R
import com.iliaskomp.filmsarecool.data.network.retrofit.TmdbService
import com.iliaskomp.filmsarecool.data.repository.FilmsRepository
import com.iliaskomp.filmsarecool.data.repository.FilmsRepositoryImpl
import com.iliaskomp.filmsarecool.ui.base.BaseFragment
import com.iliaskomp.filmsarecool.ui.popularfilms.PopularFilmsFragment

import javax.inject.Inject

class MainActivity : AppCompatActivity(), Navigator {

    @Inject
    lateinit var tmdbService: TmdbService

    val navigator = this

    lateinit var filmsRepository: FilmsRepository
        private set

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // Toolbar
        initToolbar()

        // Injection
        (application as App).netComponent.inject(this)
        filmsRepository = FilmsRepositoryImpl(tmdbService)

        // Default navigation
        navigateToPopularFilms()
    }

    private fun initToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.app_toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = getString(R.string.default_title)
    }

    override fun navigateToPopularFilms() {
        navigateTo(PopularFilmsFragment())
    }

    override fun navigateTo(fragment: BaseFragment<*, *>) {
        if (findViewById<View>(R.id.fragment_container) != null) {
            switchFragment(fragment)
        }
    }

    fun setToolbarTitle(title: String) {
        supportActionBar?.title = title
    }

    private fun switchFragment(fragment: BaseFragment<*, *>) {
        supportFragmentManager.beginTransaction()
                .add(R.id.fragment_container, fragment)
                .commit()
    }

    companion object {
        val TAG = MainActivity::class.java.name
    }
}
