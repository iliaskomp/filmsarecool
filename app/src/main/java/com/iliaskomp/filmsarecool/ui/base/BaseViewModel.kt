package com.iliaskomp.filmsarecool.ui.base


import androidx.databinding.Observable

import com.iliaskomp.filmsarecool.data.repository.FilmsRepository

abstract class BaseViewModel<L : BaseViewModelListener>(protected val listener: L) : BaseObservableViewModel() {
    private lateinit var callback: Observable.OnPropertyChangedCallback

    protected val filmsRepository: FilmsRepository
        get() = listener.filmsRepository


    fun onStart() {
        callback = initCallback()
        addOnPropertyChangedCallback(callback)
    }

    fun onStop() {
        removeOnPropertyChangedCallback(callback)
    }

    private fun initCallback(): Observable.OnPropertyChangedCallback {
        return object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable, propertyId: Int) {}
        }
    }

}
