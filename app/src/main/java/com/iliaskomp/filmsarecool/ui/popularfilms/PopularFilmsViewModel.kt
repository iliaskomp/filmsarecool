package com.iliaskomp.filmsarecool.ui.popularfilms


import com.iliaskomp.filmsarecool.data.model.PopularFilm
import com.iliaskomp.filmsarecool.ui.base.BaseViewModel
import com.iliaskomp.filmsarecool.ui.base.BaseViewModelListener

import java.util.ArrayList

import io.reactivex.Flowable
import io.reactivex.processors.PublishProcessor
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject

class PopularFilmsViewModel internal constructor(listener: BaseViewModelListener) : BaseViewModel<BaseViewModelListener>(listener) {
    private val pageSubject = PublishSubject.create<Int>()
    private val filmsProcessor = PublishProcessor.create<List<PopularFilmItemViewModel>>()
    val films: Flowable<List<PopularFilmItemViewModel>> = filmsProcessor

    init {
        pageSubject.subscribe(
                { this.startFilmsRequest(it) },
                { it.printStackTrace() }
        )
    }

    /* Fetching films =========================================================================== */
    private fun startFilmsRequest(page: Int) {
        //todo check if this page exists or the the previous was the last one
        filmsRepository
                .getPopularFilms(page)
                .map { this.mapToViewModels(it) }
                .subscribe(
                        { popularFilms -> filmsProcessor.onNext(popularFilms) },
                        { it.printStackTrace() }
                )
    }

    fun setPage(page: Int) {
        pageSubject.onNext(page)
    }

    /* Utility ================================================================================== */
    private fun mapToViewModels(popularFilms: List<PopularFilm>): List<PopularFilmItemViewModel> {
        val filmsViewModel = ArrayList<PopularFilmItemViewModel>()
        for (film in popularFilms) {
            filmsViewModel.add(PopularFilmItemViewModel(film))
        }
        return filmsViewModel
    }
}
